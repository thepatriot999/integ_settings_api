<?php

namespace JobTarget;

class Database {
	protected $db;

	public function __construct () {
		$this->db = new \PDO("dblib:host=10.52.6.21:2775;dbname=64recs67o", "coldfusion", 'pAss@$c#fo',
				array(
				\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
				\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
			     ));
	}

	public function getUsers() {
		$stmt = $this->db->prepare('
					SELECT
						username,
						password
					FROM	users
				');

		$stmt->execute();

		return $stmt->fetchAll();
	}

	public function searchSite( $criteria ) {
		$criteria2 = '%'.$criteria.'%';
        $criteria1 = intval($criteria);

		$stmt = $this->db->prepare("
					select top 10 site_id, site_name
					from p_sites
					where site_name like :criteria2
                    or site_id = :criteria1
				");

		$stmt->bindParam( ':criteria1', $criteria1, \PDO::PARAM_INT );
		$stmt->bindParam( ':criteria2', $criteria2, \PDO::PARAM_STR );
		$stmt->execute();

		return $stmt->fetchAll();

	}

	public function getTransmitters() {
		$stmt = $this->db->prepare('SELECT transmit_id, title, added FROM job_distribution_transmit where active=1 order by transmit_id desc');

		$stmt->execute();

		return $stmt->fetchAll();
	}

	public function addTransmitter($title) {
		$stmt = $this->db->prepare('
				INSERT INTO job_distribution_transmit( title, added, active )
				values( :title, GETDATE(), 1 )
				');

		$stmt->bindParam(':title', $title, \PDO::PARAM_STR);
		$stmt->execute();

		return $stmt->rowCount();
	}

	public function updateTransmitter( $info ) {
		$stmt = $this->db->prepare( '
					UPDATE job_distribution_transmit
					SET 	title = :title
					WHERE 	transmit_id = :transmit_id
				' );

		$stmt->bindParam(':title', $info->title, \PDO::PARAM_STR);
		$stmt->bindParam(':transmit_id', $info->transmit_id, \PDO::PARAM_INT);
		$stmt->execute();

		return $stmt->rowCount();
	}

	public function deleteTransmitter( $transmit_id ) {
		$stmt = $this->db->prepare('
					UPDATE job_distribution_transmit
					SET	active=0
					WHERE transmit_id = :transmit_id
				');

		$stmt->bindParam(':transmit_id', $transmit_id, \PDO::PARAM_INT);
		$stmt->execute();

		return $stmt->rowCount();
	}

	public function getFormatters() {
		$stmt = $this->db->prepare('SELECT format_id, title, added FROM job_distribution_format where active=1 order by format_id desc');

		$stmt->execute();

		return $stmt->fetchAll();
	}

	public function addFormatter($title) {
		$stmt = $this->db->prepare('
				INSERT INTO job_distribution_format( title, added, active )
				values( :title, GETDATE(), 1 )
				');

		$stmt->bindParam(':title', $title, \PDO::PARAM_STR);
		$stmt->execute();

		return $stmt->rowCount();
	}

	public function updateFormatter( $info ) {
		$stmt = $this->db->prepare( '
					UPDATE job_distribution_format
					SET 	title = :title
					WHERE 	format_id = :format_id
				' );

		$stmt->bindParam(':title', $info->title, \PDO::PARAM_STR);
		$stmt->bindParam(':format_id', $info->format_id, \PDO::PARAM_INT);
		$stmt->execute();

		return $stmt->rowCount();
	}

	public function deleteFormatter( $format_id ) {
		$stmt = $this->db->prepare('
					UPDATE job_distribution_format
					SET	active=0
					WHERE format_id = :format_id
				');

		$stmt->bindParam(':format_id', $format_id, \PDO::PARAM_INT);
		$stmt->execute();

		return $stmt->rowCount();
	}

	public function getFormatter( $site_id ) {
		$stmt = $this->db->prepare('
					SELECT 	format_site_id, format_id, site_id, added, active
					FROM	job_distribution_format_site
					WHERE	site_id = :site_id
					AND	active=1
				');

		$stmt->bindParam(':site_id', $site_id, \PDO::PARAM_INT);
		$stmt->execute();

		return $stmt->fetch();
	}

	public function getTransmitter( $site_id ) {
		$stmt = $this->db->prepare('
					SELECT 	transmit_site_id, transmit_id, site_id, added, active
					FROM	job_distribution_transmit_site
					WHERE	site_id = :site_id
					AND	active=1
				');

		$stmt->bindParam(':site_id', $site_id, \PDO::PARAM_INT);
		$stmt->execute();

		return $stmt->fetch();
	}

	public function getFormatterSettings( $format_site_id ) {
		$stmt = $this->db->prepare('
					SELECT format_setting_id, name, value, added, active
					FROM	job_distribution_format_site_setting
					WHERE	format_site_id = :format_site_id
					AND	active=1
				');

		$stmt->bindParam(':format_site_id', $format_site_id, \PDO::PARAM_INT);
		$stmt->execute();

		return $stmt->fetchAll();
	}
	
	public function getTransmitterSettings( $transmit_site_id ) {
		$stmt = $this->db->prepare('
					SELECT transmit_setting_id, name, value, added, active
					FROM	job_distribution_transmit_site_setting
					WHERE	transmit_site_id = :transmit_site_id
					AND	active=1
				');

		$stmt->bindParam(':transmit_site_id', $transmit_site_id, \PDO::PARAM_INT);
		$stmt->execute();

		return $stmt->fetchAll();
	}

	public function unsetSiteFormatter( $site_id ) {
		$stmt = $this->db->prepare('
					UPDATE 	job_distribution_format_site
					SET	active=0
					WHERE 	site_id = :site_id
					AND	active=1
				');

		$stmt->bindParam(':site_id', $site_id, \PDO::PARAM_INT);
		$stmt->execute();

		return $stmt->rowCount();
	}

	public function unsetSiteTransmitter( $site_id ) {
		$stmt = $this->db->prepare('
					UPDATE 	job_distribution_transmit_site
					SET	active=0
					WHERE 	site_id = :site_id
					AND	active=1
				');

		$stmt->bindParam(':site_id', $site_id, \PDO::PARAM_INT);
		$stmt->execute();

		return $stmt->rowCount();
	}

	public function setSiteFormatter( $site_id, $format_id ) {
		$stmt = $this->db->prepare('
					INSERT INTO job_distribution_format_site( site_id, format_id, added, active ) VALUES(:site_id, :format_id, GETDATE(), 1)
				');

		$stmt->bindParam(':site_id', $site_id, \PDO::PARAM_INT);
		$stmt->bindParam(':format_id', $format_id, \PDO::PARAM_INT);
		$stmt->execute();

		return $stmt->rowCount();
	}

	public function setSiteTransmitter( $site_id, $transmit_id ) {
		$stmt = $this->db->prepare('
					INSERT INTO job_distribution_transmit_site( site_id, transmit_id, added, active ) VALUES(:site_id, :transmit_id, GETDATE(), 1)
				');

		$stmt->bindParam(':site_id', $site_id, \PDO::PARAM_INT);
		$stmt->bindParam(':transmit_id', $transmit_id, \PDO::PARAM_INT);
		$stmt->execute();

		return $stmt->rowCount();
	}

	public function addTransmitterSetting( $transmit_site_id, $name, $value ) {
		$stmt = $this->db->prepare('
					INSERT INTO job_distribution_transmit_site_setting( transmit_site_id, name, value, added, active ) VALUES(:transmit_site_id, :name, :val, GETDATE(), 1)
				');

		$stmt->bindParam(':transmit_site_id', $transmit_site_id, \PDO::PARAM_INT);
		$stmt->bindParam(':name', $name, \PDO::PARAM_STR);
		$stmt->bindParam(':val', $value, \PDO::PARAM_STR);
		$stmt->execute();

		return $stmt->rowCount();
	}

	public function addFormatterSetting( $format_site_id, $name, $value ) {
		$stmt = $this->db->prepare('
					INSERT INTO job_distribution_format_site_setting( format_site_id, name, value, added, active ) VALUES(:format_site_id, :name, :val, GETDATE(), 1)
				');

		$stmt->bindParam(':format_site_id', $format_site_id, \PDO::PARAM_INT);
		$stmt->bindParam(':name', $name, \PDO::PARAM_STR);
		$stmt->bindParam(':val', $value, \PDO::PARAM_STR);
		$stmt->execute();

		return $stmt->rowCount();
	}

	public function updateFormatterSetting( $format_setting_id, $name, $value ) {
		$stmt = $this->db->prepare('
					UPDATE job_distribution_format_site_setting
					SET	name=:name,
						value=:val
					WHERE	format_setting_id=:format_setting_id
				');

		$stmt->bindParam(':format_setting_id', $format_setting_id, \PDO::PARAM_INT);
		$stmt->bindParam(':name', $name, \PDO::PARAM_STR);
		$stmt->bindParam(':val', $value, \PDO::PARAM_STR);
		$stmt->execute();

		return $stmt->rowCount();
	}

	public function updateTransmitterSetting( $transmit_setting_id, $name, $value ) {
		$stmt = $this->db->prepare('
					UPDATE job_distribution_transmit_site_setting
					SET	name=:name,
						value=:val
					WHERE	transmit_setting_id=:transmit_setting_id
				');

		$stmt->bindParam(':transmit_setting_id', $transmit_setting_id, \PDO::PARAM_INT);
		$stmt->bindParam(':name', $name, \PDO::PARAM_STR);
		$stmt->bindParam(':val', $value, \PDO::PARAM_STR);
		$stmt->execute();

		return $stmt->rowCount();
	}

	public function deleteTransmitterSetting( $transmit_setting_id ) {
		$stmt = $this->db->prepare('
					UPDATE job_distribution_transmit_site_setting
					SET	active=0
					WHERE transmit_setting_id = :transmit_setting_id
				');

		$stmt->bindParam(':transmit_setting_id', $transmit_setting_id, \PDO::PARAM_INT);
		$stmt->execute();

		return $stmt->rowCount();
	}

	public function deleteFormatterSetting( $format_setting_id ) {
		$stmt = $this->db->prepare('
					UPDATE job_distribution_format_site_setting
					SET	active=0
					WHERE format_setting_id = :format_setting_id
				');

		$stmt->bindParam(':format_setting_id', $format_setting_id, \PDO::PARAM_INT);
		$stmt->execute();

		return $stmt->rowCount();
	}

	public function getJobsStatuses( $postings ) {
		$stmt = $this->db->prepare('
			SELECT * from (
				select
					js.id as posting_id,
					js.job_id,
					js.site_id,
					ps.site_name,
					accountname.val as accountname,
					jss.status as job_status,
					jss.added as job_status_added,
					jdpj.packet_id,
					jdpj.added,
					ROW_NUMBER() OVER (partition by jdpj.jobsite_id order by packet_id desc) as packet_row,
					dbo.jtfn_site_feature_get(js.site_id, \'oneclick_integration_type\', \'option1\', null) AS integration_type,
					jdts.transmit_id as transmitter,
					case when jsia.jobsite_id is null then \'no_denorm_data\'
						when jsia.latest_denormalized_version = 0 then \'not_denorm\'
						when jsia.latest_denormalized_version=1 and jsia.denormalized=1 then \'denorm\' end as denorm
				from jobs_sites js WITH(NOLOCK)
				inner join p_sites ps WITH(NOLOCK) ON ps.site_id=js.site_id
				left join jobs_external_transmit_logins accountname WITH(NOLOCK) ON accountname.jobs_sites_id=js.id and accountname.active=1 and accountname.name=\'accountname\'
				left join jobs_sites_status jss WITH(NOLOCK) ON jss.jobs_sites_id=js.id and jss.active=1
				left join job_distribution_transmit_site jdts WITH(NOLOCK) ON jdts.site_id=js.site_id and jdts.active=1
				left join jobs_sites_index_available jsia WITH(NOLOCK) ON jsia.jobsite_id=js.id
				left join job_distribution_packet_job jdpj WITH(NOLOCK) ON jdpj.jobsite_id=js.id
				where js.id in ( :postings )) as tbl where tbl.packet_row=1
		');

		$stmt->bindParam(':postings', $postings, \PDO::PARAM_STR);
		$stmt->execute();

		return $stmt->rowCount();
	}
}
