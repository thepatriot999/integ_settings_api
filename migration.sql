drop database if exists distribution;

create database distribution;

use distribution;

create table p_sites
(
	site_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	site_name VARCHAR(250),
	active smallint
);

create table job_distribution_format
(
format_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
title VARCHAR(250),
added datetime,
active smallint
);

create table job_distribution_transmit
(
transmit_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
title VARCHAR(250),
added datetime,
active smallint
);

create table job_distribution_transmit_site
(
transmit_site_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
transmit_id INT,
site_id INT,
added datetime,
active smallint
);

create table job_distribution_format_site
(
format_site_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
format_id INT,
site_id INT,
added datetime,
active smallint
);

create table job_distribution_transmit_site_setting
(
transmit_setting_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
transmit_site_id INT,
name VARCHAR(250),
value VARCHAR(250),
added datetime,
active smallint
);

create table job_distribution_format_site_setting
(
format_setting_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
format_site_id INT,
name VARCHAR(250),
value VARCHAR(250),
added datetime,
active smallint
);

-- populate some sites
