<?php
header( 'Access-Control-Allow-Headers: Accept, Content-Type, Authorization' );
header( 'Access-Control-Allow-Methods: POST, PUT, DELETE, GET, OPTIONS' );

require 'vendor/autoload.php';

$corsOptions = array(
    'origin' => '*',
);

$app = new \Slim\Slim();
$app->add(new \CorsSlim\CorsSlim( $corsOptions ));

$app->get( '/', function() use ($app) {
    echo 'Home Page';
} );

$app->get('/site/search/:info', function($info) {
	$db 	= new \JobTarget\Database;
	$users 	= $db->searchSite( $info );
	echo json_encode( $users );

});

$app->get('/transmitter', function() {
	$db 	      = new \JobTarget\Database;
	$transmitters = $db->getTransmitters();
	echo json_encode($transmitters);
});

$app->post('/transmitter', function() use ($app) {
	$data = json_decode($app->request->getBody());
	$db   = new \JobTarget\Database;

	$rowCount = $db->addTransmitter( $data->title );
	echo json_encode( array( 'row_count' => $rowCount ) );
});

$app->put('/transmitter', function() use ($app) {
	$data = json_decode($app->request->getBody());
	$db   = new \JobTarget\Database;

	$rowCount = $db->updateTransmitter( $data );
	echo json_encode( array( 'row_count' => $rowCount ) );
});

$app->delete('/transmitter/:transmit_id', function($transmit_id) use ($app) {
	$db   	     = new \JobTarget\Database;

	$rowCount = $db->deleteTransmitter( $transmit_id );
	echo json_encode( array( 'row_count' => $rowCount ) );
});

$app->get('/formatter', function() {
	$db 	      = new \JobTarget\Database;
	$formatters = $db->getFormatters();
	echo json_encode($formatters);
});

$app->post('/formatter', function() use ($app) {
	$data = json_decode($app->request->getBody());
	$db   = new \JobTarget\Database;

	$rowCount = $db->addFormatter( $data->title );
	echo json_encode( array( 'row_count' => $rowCount ) );
});

$app->put('/formatter', function() use ($app) {
	$data = json_decode($app->request->getBody());
	$db   = new \JobTarget\Database;

	$rowCount = $db->updateFormatter( $data );
	echo json_encode( array( 'row_count' => $rowCount ) );
});

$app->delete('/formatter/:format_id', function($format_id) {
	$db   	     = new \JobTarget\Database;

	$rowCount = $db->deleteFormatter( $format_id );
	echo json_encode( array( 'row_count' => $rowCount ) );
});

$app->get('/formatter/settings/:site_id', function($site_id) {
	$db      = new \JobTarget\Database;
	$results = array();

	$formatter = $db->getFormatter($site_id);

	if(!empty($formatter)) {
		$results['formatter'] = $formatter;

		$formatterSettings = $db->getFormatterSettings($formatter['format_site_id']);

		if(!empty($formatterSettings)) {
			$results['formatter_settings'] = $formatterSettings;
		}
	}

	echo json_encode($results);
});

$app->get('/transmitter/settings/:site_id', function($site_id) {
	$db      = new \JobTarget\Database;
	$results = array();

	$transmitter = $db->getTransmitter($site_id);

	if(!empty($transmitter)) {
		$results['transmitter'] = $transmitter;

		$transmitterSettings = $db->getTransmitterSettings($transmitter['transmit_site_id']);

		if(!empty($transmitterSettings)) {
			$results['transmitter_settings'] = $transmitterSettings;
		}
	}

	echo json_encode($results);
});

$app->post('/formatter/settings/:site_id/:format_id', function($site_id, $format_id) {
	$db      = new \JobTarget\Database;

	$db->unsetSiteFormatter( $site_id );

	$affected = $db->setSiteFormatter( $site_id, $format_id );

	if( $affected <=0  ) {
		//$response->setStatus( 400 );
	}

	echo json_encode( array( 'affected'=>$affected ) );
});

$app->post('/transmitter/settings/:site_id/:transmit_id', function($site_id, $transmit_id) {
	$db          = new \JobTarget\Database;

	$db->unsetSiteTransmitter( $site_id );

	$affected = $db->setSiteTransmitter( $site_id, $transmit_id );

	if( $affected <=0  ) {
		//$response->setStatus( 400 );
	}

	echo json_encode( array( 'affected'=>$affected ) );
});

$app->post('/transmitter/setting', function() use ($app) {
	$data = json_decode($app->request->getBody());
	$db   = new \JobTarget\Database;

	$affected = $db->addTransmitterSetting( $data->transmit_site_id, $data->name, $data->value );

	if( $affected <=0  ) {
		//$response->setStatus( 400 );
	}

	echo json_encode( array( 'affected'=>$affected ) );
});

$app->post('/formatter/setting', function() use ($app) {
	$data = json_decode($app->request->getBody());
	$db   = new \JobTarget\Database;

	$affected = $db->addFormatterSetting( $data->format_site_id, $data->name, $data->value );

	if( $affected <=0  ) {
		$response->setStatus( 400 );
	}

	echo json_encode( array( 'affected'=>$affected ) );
});

$app->put('/formatter/setting', function() use ( $app ) {
	$data = json_decode($app->request->getBody());
	$db   = new \JobTarget\Database;

	$affected = $db->updateFormatterSetting( $data->format_setting_id, $data->name, $data->value );

	if( $affected <=0  ) {
        // TODO code to handle 404
	}

	echo json_encode( array( 'affected'=>$affected ) );
});

$app->put('/transmitter/setting', function() use ($app) {
	$data = json_decode($app->request->getBody());
	$db   = new \JobTarget\Database;

	$affected = $db->updateTransmitterSetting( $data->transmit_setting_id, $data->name, $data->value );

	if( $affected <=0  ) {
		//$response->setStatus( 400 );
	}

	echo json_encode( array( 'affected'=>$affected ) );
});

$app->delete('/transmitter/setting/:transmit_setting_id', function($transmit_setting_id) {
	$db   = new \JobTarget\Database;

	$affected = $db->deleteTransmitterSetting( $transmit_setting_id );

	if( $affected <=0  ) {
		//$response->setStatus( 400 );
	}

	echo json_encode( array( 'affected'=>$affected ) );

});

$app->delete('/formatter/setting/:format_setting_id', function($format_setting_id) {
	$db   = new \JobTarget\Database;

	$affected = $db->deleteFormatterSetting( $format_setting_id );

	if( $affected <=0  ) {
		//$response->setStatus( 400 );
	}

	echo json_encode( array( 'affected'=>$affected ) );

});

$app->run();
